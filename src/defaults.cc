// This file is generated from Makefile. Do not edit!
#include "defaults.hh"

std::string realProgramName(const std::string& name) {
  return PROGRAM_PREFIX + name + PROGRAM_SUFFIX;
}

const char* gitrevision() {
  return "88a74ff1cde22be3e894498ffd88934dc92dfef0";
}
